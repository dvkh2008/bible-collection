# Summary<a name="summary"></a>


1. [Kinh vực sâu](#kinhvucsau)
2. [Kinh Đức Chúa Thánh Thần](#kinhducchuathanhthan)
3. [Kinh Tin – Cậy – Mến](#kinhtincaymen)
4. [Kinh Ăn Năn Tội](#kinhannantoi)
5. [Suy Niệm 5 Sự Thương](#kinhsuyniem5suthuong)
5. [Suy Niệm 5 Sự Mừng](#kinhsuyniem5sumung)
6. [Kinh Lạy ơn Ðức Chúa Giêsu](#kinhlayonducchuagiesu)
7. [Kinh Lạy Nữ Vương Gia Đình](#kinhlaynuvuonggiadinh)
8. [Kinh Cám Ơn](#kinhcamon)
9. [Năm xưa trên cây sồi](#baihatnamxuatrencaysoi)
10. [Lạy mẹ maria, Mẹ thiên chúa](#baihatlaymemaria)
11. [Xin vâng](#baihatxinvang)
12. [Kinh trông cậy](#kinhtrongcay)
13. [Kinh khẩn Mẹ Hằng Cứu Giúp](#kinhkhanmehangcuugiup)


# Kinh vực sâu <a name="kinhvucsau"></a>
Lạy Chúa, con ở dưới vực sâu kêu lên Chúa, xin Chúa hãy thẩm nhậm lời con kêu van. Hãy lắng nghe tiếng con cầu xin. Nếu Chúa chấp tội nào ai rỗi được ? Bởi Chúa hằng có lòng lành, cùng vì lời Chúa phán hứa. Con đã trông cậy Chúa. Linh hồn con cậy vì lời hứa ấy, thì đã trông cậy Chúa. Những kẻ làm dân Người đêm ngày hãy trông cậy Người cho liên, vì Người rất nhân lành hay thương vô cùng, sẽ tha hết mọi tội lỗi kẻ làm dân Người thay thảy. Lạy Chúa, xin ban cho các linh hồn (… tên …) được nghỉ ngơi đời đời, và được sáng soi vô cùng. Lạy Chúa, xin cứu lấy các linh hồn cho khỏi tù ngục, mà được nghỉ yên. Amen.\
[go to Summary](#summary)

# Kinh Đức Chúa Thánh Thần <a name="kinhducchuathanhthan"></a>
Chúng con lạy ơn Đức Chúa Thánh Thần thiêng liêng sáng láng vô cùng, chúng con xin Đức Chúa Thánh Thần xuống đầy lòng chúng con, là kẻ tin cậy Đức Chúa Trời, và đốt lửa kính mến Đức Chúa Trời trong lòng chúng con; chúng con xin Đức Chúa Trời cho Đức Chúa Thánh Thần xuống.

Sửa lại mọi sự trong ngoài chúng con.

Chúng con cầu cùng Đức Chúa Trời, xưa đã cho Đức Chúa Thánh Thần xuống soi lòng dạy dỗ các Thánh Tông Đồ, thì rày chúng con cũng xin Đức Chúa Trời cho Đức Chúa Thánh Thần lại xuống an ủi dạy dỗ chúng con làm những việc lành, vì công nghiệp vô cùng Đức Chúa Giêsu Kitô là Chúa chúng con. Amen.\
[go to Summary](#summary)

# Kinh Tin – Cậy – Mến <a name="kinhtincaymen"></a>
Lạy Chúa, con tin thật có một Đức Chúa Trời là Đấng thưởng phạt vô cùng.  Con lại tin thật Đức Chúa Trời có Ba Ngôi, mà Ngôi Thứ Hai đã xuống thế làm Người, chịu nạn chịu chết mà chuộc tội cho thiên hạ.  Bấy nhiêu điều ấy cùng các điều Hội Thánh dạy thì con tin vững vàng, vì Chúa là Đấng thông minh và chân thật vô cùng đã phán truyền cho Hội Thánh.  Amen

Lạy Chúa , con trông cậy vững vàng, vì công nghiệp Đức Chúa Giêsu thì Chúa sẽ ban ơn cho con giữ đạo nên ở đời này, cho ngày sau được lên thiên đàng xem thấy mặt Đức Chúa Trời hưởng phúc đời đời, vì Chúa là đấng phép tắc và lòng lành vô cùng đã phán hứa sự ấy chẳng có lẽ nào sai được.  Amen.

Lạy Chúa , con kính mến Chúa hết lòng hết sức trên hết mọi sự, vì Chúa là Đấng trọn tốt trọn lành vô cùng, lạy vì Chúa, thì con thương yêu người ta như mình con vậy.  Amen.\
[go to Summary](#summary)

# Kinh Ăn Năn Tội <a name="kinhannantoi"></a>
Lạy Chúa con, Chúa là Đấng trọn tốt trọn lành vô cùng. Chúa đã dựng nên con, và cho Con Chúa ra đời chịu nạn chịu chết vì con, mà con đã cả lòng phản nghịch lỗi nghĩa cùng Chúa, thì con lo buồn đau đớn, cùng chê ghét mọi tội con trên hết mọi sự; con dốc lòng chừa cải, và nhờ ơn Chúa thì con sẽ lánh xa dịp tội cùng làm việc đền tội cho xứng. Amen.

O my God, I am heartily sorry for having offended you and I detest all my sins, because of your punishments, but most of all because they offend you, my God, who are all-good and deserving of all my love. I firmly resolve, with the help of your grace, to sin no more and to avoid the near occasions of sin. Amen.\
[go to Summary](#summary)

# Suy Niệm 5 Sự Thương <a name="kinhsuyniem5suthuong"></a>
**Thứ nhất thì ngắm:** Đức Chúa Giêsu lo buồn đổ mồ hôi máu. Ta hãy xin cho được ăn năn tội nên.

**Thứ hai thì ngắm:** Đức Chúa Giêsu chịu đánh đòn. Ta hãy xin cho được hãm mình chịu khó bằng lòng.

**Thứ ba thì ngắm:** Đức Chúa Giêsu chịu đội mão gai. Ta hãy xin cho được chịu mọi sự sỉ nhục bằng lòng.

**Thứ tư thì ngắm:** Đức Chúa Giêsu vác Thánh Giá. Ta hãy xin cho được vác Thánh Giá theo chân Chúa.

**Thứ năm thì ngắm:** Đức Chúa Giêsu chịu chết trên cây Thánh Giá. Ta hãy xin đóng đanh tính xác thịt vào Thánh Giá Chúa.\
[go to Summary](#summary)

# Suy Niệm 5 Sự Mừng <a name="kinhsuyniem5sumung"></a>
**Thứ nhất thì ngắm:** Đức Chúa Giêsu sống lại. Ta hãy xin cho được sống lại thật về phần linh hồn.

**Thứ hai thì ngắm:** Đức Chúa Giêsu lên trời. Ta hãy xin cho được ái mộ những sự trên trời.

**Thứ ba thì ngắm:** Đức Chúa Thánh Thần hiện xuống. Ta hãy xin cho được lòng đầy dẫy mọi ơn Đức Chúa Thánh Thần.

**Thứ tư thì ngắm:** Đức Chúa Trời cho Đức Bà lên trời. Ta hãy xin ơn chết lành trong tay Đức Mẹ.

**Thứ năm thì ngắm:** Đức Chúa Trời thưởng Đức Mẹ trên trời. Ta hãy xin Đức Mẹ phù hộ cho ta được thưởng cùng Đức Mẹ trên nước thiên đàng.\
[go to Summary](#summary)

# Kinh Lạy ơn Ðức Chúa Giêsu <a name="kinhlayonducchuagiesu"></a>
Lạy ơn Ðức Chúa Giêsu xưa bởi trời mà xuống thế gian ba mươi ba năm cùng chịu những sự thương khó cho các linh hồn được rỗi. Thì rày chúng con xin Cha rất nhân lành hay thương vô cùng, xin tha phần phạt cho các hinh hồn đã cầu nguyện hôm nay, hoặc còn giam nơi lửa luyện tội thì xin mở cửa tù rạc ấy cho ra mà đem đến chốn hưởng vui thật là nước thiên đàng vì công nghiệp Chúa con đã chịu nạn chịu chết vì chúng con. Amen.\
[go to Summary](#summary)

# Kinh Lạy Nữ Vương Gia Đình <a name="kinhlaynuvuonggiadinh"></a>
Lạy Nữ Vương gia đình, Mẹ ở đây với chúng con, vui buồn sướng khổ, Mẹ con cùng nhau chia sẻ. Xa Mẹ chúng con biết cậy trong ai? Đời chúng con gian nan khổ sở lắm, gia đình chúng con lông đông tối ngày, nhưng có Mẹ ở bên chúng con, chúng con thấy quên hết ưu phiền, vui sống qua kiếp lưu đày, mong ngày sau sung sướng cùng Mẹ muôn đời trên thiên đàng. Amen.\
[go to Summary](#summary)

# Kinh Cám Ơn <a name="kinhcamon"></a>
Con cám ơn Đức Chúa trời là Chúa lòng lành vô cùng, chẳng bỏ con, chẳng để con không đời đời mà lại sinh ra con, cho con được làm người, cùng hằng gìn giữ con hằng che chở con, lại cho ngôi hai xuống thế làm người, chuộc tội chịu chết trên cây thánh giá vì con, lại cho con được đạo thánh Đức Chúa Trời cũng chịu nhiều ơn nhiều phép hội thánh nữa, và đã cho phần xác con (ngày / đêm) hôm nay được mọi sự lành, lại cứu lấy con kẻo phải chết tươi ăn năn tội chẳng kịp. Vậy các thánh ở trên nước thiên đàng cám ơn Đức Chúa Trời thế nào thì con cùng hợp cùng các thánh mà dâng chúa con cùng cám ơn như vậy, Amen.\
[go to Summary](#summary)

# Năm xưa trên cây sồi <a name="baihatnamxuatrencaysoi"></a>
**ĐK:** Mẹ Maria ôi, Mẹ Maria ôi. Con vâng nghe Mẹ rồi, sớm chiều từ nay thống hối. Mẹ Maria ôi, xin Mẹ đoái thương nhậm lời, cho nước Việt xinh tươi, đức tin sáng ngời.

1. Năm xưa trên cây sồi làng Fatima xa xôi, Có Đức Mẹ Chúa Trời hiện ra uy linh sáng chói. Mẹ nhắn nhủ người đời hãy mau ăn năn đền bồi, Hãy tôn sùng Mẫu tâm, hãy năn lần hạt Mân côi.
 
2. Đôi môi như hoa cười Mẹ Maria vui tươi. Có biết bao lớp người gần xa đua nhau bước tới, lòng trút khỏi ngậm ngùi, mắt khô đôi suối lệ đời, ngước trông về Mẫu tâm, sống bên tình Mẹ yên vui.\
[go to Summary](#summary)

# Lạy mẹ maria, Mẹ thiên chúa <a name="baihatlaymemaria"></a>
**ÐK:** Lạy Mẹ Maria Mẹ Thiên Chúa Mẹ đồng trinh. Ðoàn con chung tiếng hát dâng tấm lòng, dâng đời sống. Lạy Mẹ Maria Mẹ nhân ái, Mẹ hiển vinh. Mẹ chính là Nữ Vương, là Trạng Sư, là Mẹ con.

1. Con dâng Mẹ đây tâm hồn, đây trí khôn, cả dĩ vãng cả hiện tại với tương lai. Ðức thiện toàn con cương quyết gắng đi tới. Trông lên Mẹ là gương mẫu của đời con.

2. Yêu thanh bần, yêu vâng lời, yêu khiết trinh, và yêu sống trên con đường Chúa đi xưa. Xứng con Mẹ con vui bước tới Thiên Chúa. Hy sinh nhiều vì bác ái quên lợi danh.

3. Con nguyện cầu, con trung thành con quyết tâm. Mẹ nhận lấy cả tâm hồn con kính dâng. Sống bên Mẹ muôn ơn thánh giúp con tiến. Xin che chở giờ sau hết qua trần gian.\
[go to Summary](#summary)

# Xin vâng <a name="baihatxinvang"></a>
**ÐK:** Xin vâng, Mẹ dạy con hai tiếng ‘Xin Vâng’, hôm qua, hôm nay và ngày mai. Xin vâng, Mẹ dạy con hai tiếng ‘Xin Vâng’, hôm nay, tương lai và suốt đời.

1. Mẹ ơi đời con dõi bước theo Mẹ, lòng con quyết noi gương Mẹ, xin Mẹ dạy con hai tiếng: ‘Xin Vâng’. Mẹ ơi đường đi trăm ngàn nguy khó, hiểm nguy dâng tràn đây đó, xin Mẹ dạy con hai tiếng: ‘Xin Vâng’.

2. Mẹ ơi đời con dõi bước theo Mẹ, lòng con quyết noi gương Mẹ, xin Mẹ dạy con hai tiếng: ‘Xin Vâng’. Mẹ ơi đường đi phủ đầy bóng tối, bẫy chông giăng tràn muôn lối, xin Mẹ dạy con hai tiếng: ‘Xin Vâng’.

3. Mẹ ơi, Mẹ tin nơi Chúa an bài, Mẹ nên phúc ân muôn loài. Nơi Mẹ đoàn con tuôn đến nương thân. Mẹ ơi, niềm vui bên Mẹ rạng rỡ, lòng con sao còn bỡ ngỡ ? Xin Mẹ dạy con hai tiếng: ‘Xin Vâng’.\
[go to Summary](#summary)

# Kinh trông cậy <a name="kinhtrongcay"></a>
Chúng con trông cậy rất thánh Đức Mẹ Chúa Trời, xin chớ chê chớ bỏ lời con nguyện, trong cơn gian nan thiếu thốn, Đức Nữ Đồng Trinh hiển vinh sáng láng hằng chữa chúng con cho khỏi mọi sự dữ, Amen.

**CÁC CÂU LẠY**

> **Thưa**: Lạy rất thánh Trái Tim Đức Chúa Giêsu.\
> **Đáp**: Thương xót chúng con.

> **Thưa**: Lạy Trái Tim cực Thánh cực tịnh Rất Thánh Đức Bà Maria.\
> **Đáp**: Cầu cho chúng con.

> **Thưa**: Lạy Ông thánh Giuse là bạn thanh sạch Đức Bà Maria trọn đời đồng trinh.\
> **Đáp**: Cầu cho chúng con.

> **Thưa**: Các Thánh Tử Vì Đạo nước Việt Nam.\
> **Đáp**: Cầu cho chúng con.

> **Thưa**: Nữ Vương ban sự bằng an.\
> **Đáp**: Cầu cho chúng con.

[go to Summary](#summary)

# Kinh khẩn Mẹ Hằng Cứu Giúp <a name="kinhkhanmehangcuugiup"></a>
Lạy Mẹ Hằng Cứu Giúp, xin che chở mọi kẻ thuộc về con, cho Đức Giáo hoàng, cho Hội Thánh, cho nước Việt Nam con, cho gia đình con, cho kẻ thân nghĩa, kẻ thù nghịch và hết mọi kẻ khốn khó. Sau hết cho các Linh Hồn đáng thương trong Lửa Luyện Ngục.
Thánh Maria, lạy Mẹ Hằng Cứu Giúp, xin cầu bầu cho con. Lạy Thánh Anphongxô là quan thầy bào chữa con, xin giúp đỡ con trong những khi khốn khó biết chạy đến cùng Đức Bà Maria.
Lạy Đức Mẹ Hằng Cứu Giúp, cầu cho chúng con. 
Lạy Thánh Anphongxô, cầu cho chúng con.\
Amen.\
[go to Summary](#summary)